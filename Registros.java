package simulacion_registros;
import javax.swing.JTextArea;
/**
 *
 * @author German
 */
public class Registros {

    public String AX[], BX[], CX[], DX[];
    public int a[];

    public Registros() {

        AX = new String[16];
        BX = new String[16];
        CX = new String[16];
        DX = new String[16];
        a = new int[50];

    }

    public String Suma_Binaria(String binario1, String binario2) {
        if (binario1 == null || binario2 == null) {
            return "";
        }
        int primer_elemento = binario1.length() - 1;
        int segundo_elemento = binario2.length() - 1;
        StringBuilder sb = new StringBuilder();
        int acarreado = 0;
        while (primer_elemento >= 0 || segundo_elemento >= 0) {
            int sum = acarreado;
            if (primer_elemento >= 0) {
                sum += binario1.charAt(primer_elemento) - '0';
                primer_elemento--;
            }
            if (segundo_elemento >= 0) {
                sum += binario2.charAt(segundo_elemento) - '0';
                segundo_elemento--;
            }
            acarreado = sum >> 1;
            sum = sum & 1;
            sb.append(sum == 0 ? '0' : '1');
        }
        if (acarreado > 0) {

            sb.append('1');
        }

        sb.reverse();
        return String.valueOf(sb);
    }

    public String conversion(int numero_natural) {

        JTextArea area = new JTextArea();
        int d, i, j;
        for (i = 0; numero_natural != 1; i++) {
            d = numero_natural % 2;
            a[i] = d;
            numero_natural = numero_natural / 2;
        }
        a[i] = 1;
        for (j = i; j >= 0; --j) {
            area.append("" + a[j]);
        }
        String n = area.getText();
        return n;
    }

    public void reg() {

        for (int i = 0; i < AX.length; i++) {
            AX[i] = "0";
        }
        for (int i = 0; i < BX.length; i++) {
            BX[i] = "0";
        }
        for (int i = 0; i < CX.length; i++) {
            CX[i] = "0";
        }
        for (int i = 0; i < DX.length; i++) {
            DX[i] = "0";
        }
    }

    public void imprime() {

        for (int i = 0; i < AX.length; i++) {
            System.out.print("" + AX[i]);

        }
        System.out.println();
        for (int i = 0; i < BX.length; i++) {
            System.out.print("" + BX[i]);

        }
        System.out.println();
        for (int i = 0; i < CX.length; i++) {
            System.out.print("" + CX[i]);

        }
        System.out.println();
        for (int i = 0; i < DX.length; i++) {
            System.out.print("" + DX[i]);

        }
    }

    public void guardar(String registro, int decimal) {
        String convertido = conversion(decimal);

        if (registro.equalsIgnoreCase("AL") && decimal <= 255) {

            int contador = convertido.length();

            for (int i = AX.length - 1; i >= AX.length - convertido.length(); i--) {
                AX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal <= 255) {

            int contador = convertido.length();

            for (int i = AX.length - 9; i >= (AX.length - 8) - convertido.length(); i--) {
                AX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("AX") && decimal <= 65535) {

            int contador = convertido.length();

            for (int i = AX.length - 1; i >= (AX.length) - convertido.length(); i--) {
                AX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }

        }
        if (registro.equalsIgnoreCase("BL") && decimal <= 255) {

            int contador = convertido.length();

            for (int i = BX.length - 1; i >= BX.length - convertido.length(); i--) {
                BX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal <= 255) {

            int contador = convertido.length();

            for (int i = BX.length - 9; i >= (BX.length - 8) - convertido.length(); i--) {
                BX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("BX") && decimal <= 65535) {

            int contador = convertido.length();

            for (int i = BX.length - 1; i >= (BX.length) - convertido.length(); i--) {
                BX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }

        }
        if (registro.equalsIgnoreCase("CL") && decimal <= 255) {

            int contador = convertido.length();

            for (int i = CX.length - 1; i >= CX.length - convertido.length(); i--) {
                CX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal <= 255) {

            int contador = convertido.length();

            for (int i = CX.length - 9; i >= (CX.length - 8) - convertido.length(); i--) {
                CX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("CX") && decimal <= 65535) {

            int contador = convertido.length();

            for (int i = CX.length - 1; i >= (CX.length) - convertido.length(); i--) {
                CX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }

        }
        if (registro.equalsIgnoreCase("DL") && decimal <= 255) {

            int contador = convertido.length();

            for (int i = DX.length - 1; i >= DX.length - convertido.length(); i--) {
                DX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal <= 255) {

            int contador = convertido.length();

            for (int i = DX.length - 9; i >= (DX.length - 8) - convertido.length(); i--) {
                DX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("DX") && decimal <= 65535) {

            int contador = convertido.length();

            for (int i = DX.length - 1; i >= (DX.length) - convertido.length(); i--) {
                DX[i] = convertido.charAt(contador - 1) + "";
                contador--;
            }
        }
    }

    public String obtener_al() {

        StringBuilder area = new StringBuilder();

        for (int i = AX.length - 1; i >= AX.length - 8; i--) {
            area.append(AX[i]);
        }
        String a = area.reverse() + "";


        return a;
    }

    public String obtener_ah() {

        StringBuilder area = new StringBuilder();

        for (int i = AX.length - 9; i >= 0; i--) {
            area.append(AX[i]);
        }
        String a = area.reverse() + "";

        return a;
    }

    public String obtener_ax() {

        StringBuilder area = new StringBuilder();

        for (int i = AX.length - 1; i >= 0; i--) {
            area.append(AX[i]);
        }
        String a = area.reverse() + "";

        return a;
    }

    public String obtener_bl() {

        StringBuilder area = new StringBuilder();

        for (int i = BX.length - 1; i >= BX.length - 8; i--) {
            area.append(BX[i]);
        }
        String a = area.reverse() + "";


        return a;
    }

    public String obtener_bh() {

        StringBuilder area = new StringBuilder();

        for (int i = BX.length - 9; i >= 0; i--) {
            area.append(BX[i]);
        }
        String a = area.reverse() + "";


        return a;
    }

    public String obtener_bx() {

        StringBuilder area = new StringBuilder();

        for (int i = BX.length - 1; i >= 0; i--) {
            area.append(BX[i]);
        }
        String a = area.reverse() + "";


        return a;
    }

    public String obtener_cl() {

        StringBuilder area = new StringBuilder();

        for (int i = CX.length - 1; i >= CX.length - 8; i--) {
            area.append(CX[i]);
        }
        String a = area.reverse() + "";

        return a;
    }

    public String obtener_ch() {

        StringBuilder area = new StringBuilder();

        for (int i = CX.length - 9; i >= 0; i--) {
            area.append(CX[i]);
        }
        String a = area.reverse() + "";


        return a;
    }

    public String obtener_cx() {

        StringBuilder area = new StringBuilder();

        for (int i = CX.length - 1; i >= 0; i--) {
            area.append(CX[i]);
        }
        String a = area.reverse() + "";


        return a;
    }

    public String obtener_dl() {

        StringBuilder area = new StringBuilder();

        for (int i = DX.length - 1; i >= DX.length - 8; i--) {
            area.append(DX[i]);
        }
        String a = area.reverse() + "";


        return a;
    }

    public String obtener_dh() {

        StringBuilder area = new StringBuilder();

        for (int i = DX.length - 9; i >= 0; i--) {
            area.append(DX[i]);
        }
        String a = area.reverse() + "";


        return a;
    }

    public String obtener_dx() {

        StringBuilder area = new StringBuilder();

        for (int i = DX.length - 1; i >= 0; i--) {
            area.append(DX[i]);
        }
        String a = area.reverse() + "";

        return a;
    }

    public void sumar(String registro, int decimal) {
        String convertido = conversion(decimal);

        if (registro.equalsIgnoreCase("AL") && decimal <= 255) {

            String nuevo = Suma_Binaria(obtener_al(), convertido);

            int contador = nuevo.length();

            for (int i = AX.length - 1; i >= AX.length - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("AH") && decimal <= 255) {

            String nuevo = Suma_Binaria(obtener_ah(), convertido);

            int contador = nuevo.length();

            for (int i = AX.length - 9; i >= (AX.length - 8) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("AX") && decimal <= 65535) {

            String nuevo = Suma_Binaria(obtener_ax(), convertido);

            int contador = nuevo.length();

            for (int i = AX.length - 1; i >= (AX.length) - nuevo.length(); i--) {
                AX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }


        if (registro.equalsIgnoreCase("BL") && decimal <= 255) {

            String nuevo = Suma_Binaria(obtener_bl(), convertido);

            int contador = nuevo.length();

            for (int i = BX.length - 1; i >= BX.length - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("BH") && decimal <= 255) {

            String nuevo = Suma_Binaria(obtener_bh(), convertido);

            int contador = nuevo.length();

            for (int i = BX.length - 9; i >= (BX.length - 8) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("BX") && decimal <= 65535) {

            String nuevo = Suma_Binaria(obtener_bx(), convertido);

            int contador = nuevo.length();

            for (int i = BX.length - 1; i >= (BX.length) - nuevo.length(); i--) {
                BX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("CL") && decimal <= 255) {

            String nuevo = Suma_Binaria(obtener_cl(), convertido);

            int contador = nuevo.length();

            for (int i = CX.length - 1; i >= CX.length - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("CH") && decimal <= 255) {

            String nuevo = Suma_Binaria(obtener_ch(), convertido);

            int contador = nuevo.length();

            for (int i = CX.length - 9; i >= (CX.length - 8) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("CX") && decimal <= 65535) {

            String nuevo = Suma_Binaria(obtener_cx(), convertido);

            int contador = nuevo.length();

            for (int i = CX.length - 1; i >= (CX.length) - nuevo.length(); i--) {
                CX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("DL") && decimal <= 255) {

            String nuevo = Suma_Binaria(obtener_dl(), convertido);

            int contador = nuevo.length();

            for (int i = DX.length - 1; i >= DX.length - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
        if (registro.equalsIgnoreCase("DH") && decimal <= 255) {

            String nuevo = Suma_Binaria(obtener_dh(), convertido);

            int contador = nuevo.length();

            for (int i = DX.length - 9; i >= (DX.length - 8) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }

        if (registro.equalsIgnoreCase("DX") && decimal <= 65535) {

            String nuevo = Suma_Binaria(obtener_dx(), convertido);

            int contador = nuevo.length();

            for (int i = DX.length - 1; i >= (DX.length) - nuevo.length(); i--) {
                DX[i] = nuevo.charAt(contador - 1) + "";
                contador--;
            }
        }
    }

    public static void main(String[] args) {
        Registros e = new Registros();
        e.reg();
        System.out.println();
        e.guardar("AL", 10);
        e.guardar("AH", 100);
        e.guardar("BL", 10);
        e.guardar("BH", 100);
        e.guardar("CL", 10);
        e.guardar("CH", 100);
        e.guardar("DL", 10);
        e.guardar("DH", 100);
        e.sumar("AL", 1);
        e.sumar("AH", 1);
        e.sumar("BL", 1);
        e.sumar("BH", 1);
        e.sumar("CL", 1);
        e.sumar("CH", 1);
        e.sumar("DL", 1);
        e.sumar("DH", 1);
        e.imprime();

    }
}
